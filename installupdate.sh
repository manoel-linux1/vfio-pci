#!/bin/bash

clear

if [[ $EUID -eq 0 ]]; then
echo " ███████ ██████  ██████   ██████  ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██ ██ "
echo " █████   ██████  ██████  ██    ██ ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██    "
echo " ███████ ██   ██ ██   ██  ██████  ██   ██ ██ "                                                                                        
echo "#################################################################"
echo "(This script should not be executed as a superuser or sudo)"
echo "(Please run it without superuser privileges or sudo)"
echo "#################################################################"
exit 1
fi

clear

wget https://gitlab.com/manoel-linux1/my-config-lxqt/-/raw/master/installupdate-kernel-custom

clear

echo "#################################################################"

sudo rm -rf /etc/chrony/chrony.conf

sudo rm -rf /etc/modprobe.d/disable-gpu.conf

sudo rm -rf /usr/bin/installupdate-kernel-custom

sudo rm -rf /usr/bin/mini-opti

sudo rm -rf /usr/bin/opti-kernel

sudo rm -rf /usr/bin/opti-wayfire

sudo rm -rf /usr/bin/startbrowser

sudo rm -rf /usr/bin/startvirtmanager

sudo rm -rf /usr/bin/startvm-linux-1

sudo rm -rf /usr/bin/startvm-linux-2

sudo rm -rf /usr/bin/startvm-windows-1

sudo rm -rf /usr/bin/startvm-windows-2

sudo rm -rf /usr/bin/startweston

sudo rm -rf /etc/sysctl.conf

sudo rm -rf /usr/bin/vfio-pci-bind

sudo rm -rf /usr/bin/wayfire-tweaks

rm -rf ~/weston.ini

echo "#################################################################"

clear

echo "#################################################################"

sudo cp chrony.conf /etc/chrony/

sudo cp disable-gpu.conf /etc/modprobe.d/

sudo cp installupdate-kernel-custom /usr/bin/

sudo cp mini-opti /usr/bin/

sudo cp opti-kernel /usr/bin/

sudo cp opti-wayfire /usr/bin/

sudo cp startbrowser /usr/bin/

sudo cp startvirtmanager /usr/bin/

sudo cp startvm-linux-1 /usr/bin/

sudo cp startvm-linux-2 /usr/bin/

sudo cp startvm-windows-1 /usr/bin/

sudo cp startvm-windows-2 /usr/bin/

sudo cp startweston /usr/bin/

sudo cp sysctl.conf /etc/

sudo cp vfio-pci-bind /usr/bin/

sudo cp wayfire-tweaks /usr/bin/

cp weston.ini ~/weston.ini

echo "#################################################################"

clear

echo "#################################################################"

sudo chmod +x /usr/bin/installupdate-kernel-custom

sudo chmod +x /usr/bin/mini-opti

sudo chmod +x /usr/bin/opti-kernel

sudo chmod +x /usr/bin/opti-wayfire

sudo chmod +x /usr/bin/startbrowser

sudo chmod +x /usr/bin/startvirtmanager

sudo chmod +x /usr/bin/startvm-linux-1

sudo chmod +x /usr/bin/startvm-linux-2

sudo chmod +x /usr/bin/startvm-windows-1

sudo chmod +x /usr/bin/startvm-windows-2

sudo chmod +x /usr/bin/startweston

sudo chmod +x /usr/bin/vfio-pci-bind

sudo chmod +x /usr/bin/wayfire-tweaks

echo "#################################################################"

sudo apk update && sudo apk upgrade

sudo apk del xf86-video-intel

sudo apk del xf86-video-nouveau

sudo apk del xf86-video-amdgpu

sudo apk del xf86-video-ati

sudo apk del xf86-video-vesa

sudo apk del xf86-video-fbdev

sudo apk update && sudo apk upgrade

sudo apk add font-dejavu mint-y-theme mint-y-icons wireplumber-logind vulkan-tools qt5-qtwayland qt6-qtwayland util-linux-misc binutils xz pciutils 7zip xdg-user-dirs xdg-utils weston weston-backend-drm weston-shell-desktop xfce4-terminal thunar tar flatpak

sudo apk update && sudo apk upgrade

sudo apk add lang

sudo apk update && sudo apk upgrade

sudo apk fix linux-edge

sudo apk update && sudo apk upgrade

sudo apk fix

clear

flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

clear

echo "#################################################################"

sudo grub-mkconfig -o /boot/grub/grub.cfg

echo "#################################################################"

clear

cd $HOME

clear

sudo rm -rf /usr/share/edk2-debian

sudo mkdir -p /usr/share/edk2-debian

mkdir ovmf

cd ovmf

wget http://ftp.us.debian.org/debian/pool/main/e/edk2/ovmf_2024.05-1_all.deb

ar xv ovmf_2024.05-1_all.deb

tar -xvf data.tar.xz

sudo cp usr/share/OVMF/*4M* /usr/share/edk2-debian/

cd ..

sudo rm -rf ovmf

clear

echo "#################################################################"
echo " ██████   ██████  ███    ██ ███████ ██ "
echo " ██   ██ ██    ██ ████   ██ ██      ██ "
echo " ██   ██ ██    ██ ██ ██  ██ █████   ██ "
echo " ██   ██ ██    ██ ██  ██ ██ ██         "
echo " ██████   ██████  ██   ████ ███████ ██ "  
echo "#################################################################"